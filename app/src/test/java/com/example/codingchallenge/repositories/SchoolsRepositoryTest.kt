@file:OptIn(ExperimentalCoroutinesApi::class)

package com.example.codingchallenge.repositories

import com.example.codingchallenge.api.SchoolsApi
import com.example.codingchallenge.data.School
import junit.framework.TestCase.assertTrue
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when`

class SchoolsRepositoryTest {

    private val dispatcher = UnconfinedTestDispatcher()

    @Before
    fun prepare() {

        Dispatchers.setMain(dispatcher)
    }

    @Test
    fun whenGettingAllSchoolsSuccess_thenReturnSchools() = runTest(dispatcher) {
        val mockApi = mock(SchoolsApi::class.java)
        `when`(mockApi.getAllSchools()).thenReturn(listOf(mock()))
        val repo = SchoolsRepositoryImpl(mockApi)

        launch {
            val result: List<School> = repo.getAllSchools()
            assertTrue(result.isNotEmpty())
        }
    }

    @Test
    fun whenGettingAllSchoolsSuccess_thenEmptyList() = runTest(dispatcher) {
        val mockApi = mock(SchoolsApi::class.java)
        `when`(mockApi.getAllSchools()).thenReturn(listOf())
        val repo = SchoolsRepositoryImpl(mockApi)

        launch {
            val result: List<School> = repo.getAllSchools()
            assertTrue(result.isEmpty())
        }
    }

    @Test
    fun whenCachingSchool_thenFetchSchoolIsSuccessful() = runTest(dispatcher) {
        val mockApi = mock(SchoolsApi::class.java)
        val repo = SchoolsRepositoryImpl(mockApi)
        val initialState = repo.selectedSchoolCache

        repo.selectedSchoolCache = mock()
        val newResult = repo.selectedSchoolCache

        assertTrue(newResult != initialState)
    }
}