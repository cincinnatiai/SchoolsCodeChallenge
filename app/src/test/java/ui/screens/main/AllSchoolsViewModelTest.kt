package ui.screens.main

import com.example.codingchallenge.data.School
import com.example.codingchallenge.repositories.SchoolsRepository
import com.example.codingchallenge.ui.screens.main.AllSchoolsViewModel
import junit.framework.TestCase.assertTrue
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.advanceUntilIdle
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when`

@ExperimentalCoroutinesApi
class AllSchoolsViewModelTest {

    private val dispatcher = UnconfinedTestDispatcher()

    @Before
    fun prepare() {
        Dispatchers.setMain(dispatcher)
    }

    @After
    fun destroy() {
        Dispatchers.resetMain()
    }

    @Test
    fun whenGettingAllSchoolsSuccess_thenReturnSchools() = runTest(dispatcher) {
        val mockRepo = mock(SchoolsRepository::class.java)
        `when`(mockRepo.getAllSchools()).thenReturn(listOf(mock()))
        val viewModel = AllSchoolsViewModel(mockRepo)
        val initialState = viewModel.allSchoolsStateFlow.value

        launch {
            viewModel.getAllSchools()
        }
        advanceUntilIdle()

        val result: List<School> = viewModel.allSchoolsStateFlow.value
        assertTrue(result.isNotEmpty())
        assertTrue(result != initialState)
    }

    @Test
    fun whenGettingAllSchoolsSuccess_thenEmptyListEmitted() = runTest(dispatcher) {
        val mockRepo = mock(SchoolsRepository::class.java)
        `when`(mockRepo.getAllSchools()).thenReturn(listOf())
        val viewModel = AllSchoolsViewModel(mockRepo)
        val initialState = viewModel.allSchoolsStateFlow.value

        launch {
            viewModel.getAllSchools()
            val result: List<School> = viewModel.allSchoolsStateFlow.value
            assertTrue(result.isEmpty())
            assertTrue(result == initialState)
        }
    }
}