package com.example.codingchallenge.ui.screens.details

import androidx.lifecycle.ViewModel
import com.example.codingchallenge.data.School
import com.example.codingchallenge.dependencies.DependencyGraph.schoolsRepository
import com.example.codingchallenge.repositories.SchoolsRepository

class DetailsScreenViewModel(
    private val repository: SchoolsRepository = schoolsRepository
) : ViewModel() {

    fun fetchLastSelectedSchool(): School {
        // To get to this screen, school should already have been saved.
        return repository.selectedSchoolCache!!
    }
}