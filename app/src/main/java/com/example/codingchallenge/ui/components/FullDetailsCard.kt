package com.example.codingchallenge.ui.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.codingchallenge.data.School

@Composable
fun FullDetailsCard(schoolToDisplay: School) {
    Column(
        Modifier
            .fillMaxWidth()
            .padding(8.dp)
            .background(Color.LightGray)
            .padding(12.dp)
    ) {
        Text(
            text = schoolToDisplay.name,
            modifier = Modifier.padding(vertical = 12.dp),
            fontWeight = FontWeight.ExtraBold,
            fontSize = 32.sp
        )

        Text(
            text = "School ID: ${schoolToDisplay.id}",
            modifier = Modifier.padding(vertical = 12.dp),
            fontWeight = FontWeight.Bold,
            fontSize = 20.sp
        )

        Text(
            text = "It's located at: ${schoolToDisplay.location}",
            modifier = Modifier.padding(vertical = 12.dp),
            fontWeight = FontWeight.Medium,
            fontSize = 20.sp
        )

        Text(
            text = schoolToDisplay.description,
            modifier = Modifier.padding(vertical = 12.dp),
            fontSize = 16.sp
        )
    }
}

@Preview
@Composable
fun PreviewFullDetailsCard() {
    val sampleSchool = School(
        id = "123134",
        name = "NYC HighSchool",
        description = "We've filmed many movies here!",
        location = "NY City"
    )
    FullDetailsCard(sampleSchool)
}