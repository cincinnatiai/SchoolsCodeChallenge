package com.example.codingchallenge.ui.screens.details

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import com.example.codingchallenge.ui.components.FullDetailsCard

@Composable
fun SchoolDetailsScreen(
    navController: NavHostController,
    viewModel: DetailsScreenViewModel
) {
    val schoolToDisplay = viewModel.fetchLastSelectedSchool()
    FullDetailsCard(schoolToDisplay)
}
