package com.example.codingchallenge.ui.components

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.codingchallenge.R
import com.example.codingchallenge.data.School

@Composable
fun SchoolCard(
    school: School,
    onClick: () -> Unit
) {
    Column(
        Modifier
            .fillMaxWidth()
            .clickable { onClick() }
            .padding(8.dp)
            .background(Color.LightGray)
            .padding(12.dp)
    ) {
        Text(
            text = stringResource(
                R.string.school_header_text,
                school.name,
                school.id,
                school.location
            ),
            modifier = Modifier.padding(vertical = 12.dp),
            fontWeight = FontWeight.Bold,
            fontSize = 20.sp
        )

        Text(
            text = school.description,
            modifier = Modifier.padding(vertical = 12.dp),
            fontSize = 10.sp
        )
    }
}

@Preview
@Composable
fun PreviewSampleSchoolCard() {
    val sampleSchool = School(
        id = "123134",
        name = "NYC HighSchool",
        description = "We've filmed many movies here!",
        location = "NY City"
    )
    SchoolCard(school = sampleSchool, onClick = {})
}