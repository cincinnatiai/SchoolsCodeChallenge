package com.example.codingchallenge.ui.screens.main

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.example.codingchallenge.NavigationScreens.detailsScreen
import com.example.codingchallenge.data.School
import com.example.codingchallenge.ui.components.SchoolCard

@Composable
fun AllSchoolsScreen(
    navController: NavHostController,
    viewModel: AllSchoolsViewModel
) {

    val stateSchools = viewModel.allSchoolsStateFlow.collectAsState().value
    viewModel.getAllSchools()
    if (stateSchools.isEmpty()) {
        LoadingScreen()
    } else {
        AllSchoolsList(stateSchools, onSeeDetailsClicked = {
            viewModel.cacheSelectedSchool(it)
            navController.navigate(detailsScreen)
        })
    }
}

@Composable
fun AllSchoolsList(stateSchools: List<School>, onSeeDetailsClicked: (School) -> Unit) {
    LazyColumn(
        Modifier
            .fillMaxSize()
            .background(Color.Black)
            .padding(32.dp)
    ) {
        items(stateSchools) { school ->
            SchoolCard(school = school) {
                onSeeDetailsClicked(school)
            }
        }
    }
}

@Composable
fun LoadingScreen() {
    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        CircularProgressIndicator(
            modifier = Modifier.size(64.dp)
        )
    }
}