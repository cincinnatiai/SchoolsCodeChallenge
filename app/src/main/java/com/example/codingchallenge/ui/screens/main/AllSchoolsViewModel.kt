package com.example.codingchallenge.ui.screens.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.codingchallenge.data.School
import com.example.codingchallenge.dependencies.DependencyGraph.schoolsRepository
import com.example.codingchallenge.repositories.SchoolsRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

class AllSchoolsViewModel(
    private val repository: SchoolsRepository = schoolsRepository
) : ViewModel() {

    val allSchoolsStateFlow = MutableStateFlow<List<School>>(emptyList())

    fun getAllSchools() {
        viewModelScope.launch {
            val newSchools = repository.getAllSchools()
            allSchoolsStateFlow.emit(newSchools)
        }
    }

    fun cacheSelectedSchool(school: School) {
        repository.selectedSchoolCache = school
    }
}