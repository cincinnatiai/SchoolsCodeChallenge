package com.example.codingchallenge

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.codingchallenge.ui.screens.details.DetailsScreenViewModel
import com.example.codingchallenge.ui.screens.details.SchoolDetailsScreen
import com.example.codingchallenge.ui.screens.main.AllSchoolsScreen
import com.example.codingchallenge.ui.screens.main.AllSchoolsViewModel
import com.example.codingchallenge.ui.theme.CodingChallengeTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CodingChallengeTheme {
                val navHostController = rememberNavController()

                NavHost(
                    navController = navHostController,
                    startDestination = NavigationScreens.previewScreen
                ) {
                    composable(NavigationScreens.previewScreen) {
                        val allSchoolsViewModel by viewModels<AllSchoolsViewModel>()
                        AllSchoolsScreen(
                            navController = navHostController,
                            viewModel = allSchoolsViewModel
                        )
                    }
                    composable(NavigationScreens.detailsScreen) {
                        val detailsViewModel by viewModels<DetailsScreenViewModel>()
                        SchoolDetailsScreen(
                            navController = navHostController,
                            viewModel = detailsViewModel
                        )
                    }
                }
            }
        }
    }
}

object NavigationScreens {
    const val previewScreen = "previewScreen"
    const val detailsScreen = "detailsScreen"
}
