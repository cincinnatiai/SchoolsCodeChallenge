package com.example.codingchallenge.repositories

import com.example.codingchallenge.api.SchoolsApi
import com.example.codingchallenge.data.School
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

interface SchoolsRepository {
    suspend fun getAllSchools(): List<School>
    var selectedSchoolCache: School?
}

class SchoolsRepositoryImpl(
    private val api: SchoolsApi,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) : SchoolsRepository {

    override suspend fun getAllSchools(): List<School> {
        return withContext(dispatcher) {
            return@withContext api.getAllSchools()
        }
    }

    override var selectedSchoolCache: School? = null
}

class MockRepo : SchoolsRepository {
    val sampleSchool = School(
        id = "123134",
        name = "NYC HighSchool",
        description = "We've filmed many movies here!",
        location = "NY City"
    )

    override suspend fun getAllSchools(): List<School> {
        return listOf(sampleSchool, sampleSchool.copy(name = "Another NYC School"))
    }

    override var selectedSchoolCache: School? = sampleSchool
}