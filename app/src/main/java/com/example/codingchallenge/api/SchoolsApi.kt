package com.example.codingchallenge.api

import com.example.codingchallenge.data.School
import retrofit2.http.GET

/**
 * https://data.cityofnewyork.us/resource/s3k6-pzi2.json
 */
interface SchoolsApi {
    companion object {
        const val BASE_URL = "https://data.cityofnewyork.us/"
        const val ALL_SCHOOLS_ENDPOINT = "resource/s3k6-pzi2.json"
    }

    @GET(ALL_SCHOOLS_ENDPOINT)
    suspend fun getAllSchools(): List<School>
}
