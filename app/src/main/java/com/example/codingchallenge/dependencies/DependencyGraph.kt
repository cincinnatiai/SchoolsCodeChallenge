package com.example.codingchallenge.dependencies

import com.example.codingchallenge.api.SchoolsApi
import com.example.codingchallenge.repositories.SchoolsRepository
import com.example.codingchallenge.repositories.SchoolsRepositoryImpl
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


// TODO: Add a proper dependency injection framework like Dagger2 or Hilt.
object DependencyGraph {

    val schoolsRepository: SchoolsRepository by lazy {
        SchoolsRepositoryImpl(schoolsApi)
    }

    private val schoolsApi: SchoolsApi by lazy {
        retrofitInstance.create(SchoolsApi::class.java)
    }

    private val retrofitInstance by lazy {
        Retrofit
            .Builder()
            .addConverterFactory(converterFactory)
            .baseUrl(SchoolsApi.BASE_URL)
            .build()
    }

    private val converterFactory by lazy {
        GsonConverterFactory.create()
    }
}